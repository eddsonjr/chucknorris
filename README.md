# ChuckNorrisFunFact

Aplicativo desenvolvido em Kotlin cujo objetivo é consumir a API chucknorris.io. 

## Sobre a API chucknorris.io

**chucknorris.io** é uma API baseada em JSON cujo o objetivo é disponibilizar pequenos textos satíricos (fun facts) sobre o ator Chuck Norris.  Ela é capaz de retornar um único texto satírico de forma randômica, fazer buscas por textos satíricos baseado em uma lista de categorias ou por algum texto qualquer que lhe é fornecida.   
  
Para maiores detalhes, consulte a [documentação](https://api.chucknorris.io/)

## Apresentação da Aplicação

A aplicação conta com as seguintes funcionalidades: 

1. **Somente um Joke** - Apresenta ao usuário um único texto satírico sobre Chuck Noris por vez. Este texto é gerado de forma randômica pela API e o usuário pode requisitar a geração de mais de uma *fun fact*.

2. **Lista de Categorias de Jokes** - Solicita da API a lista de categorias nos quais uma *fun fact* pode ser gerada. Ao selecionar uma das categorias, o usuário poderá requisitar *fun facts*  baseadas naquela categoria específica.

3. **Procurar Jokes por Texto** - Nesta funcionalidade, o usuário pode solicitar uma lista de *fun facts* baseadas em uma pesquisa por texto livre. Por exemplo, caso o usuário procure pela palavra *dog*, o aplicativo irá solicitar da API que retorne uma lista de *fun facts* baseados no texto inserido. 

## Libs Utilizadas

O projeto faz uso das seguintes libs: 

1. **Retrofit** - Biblioteca para trabalhar com requisições web, geralmente REST API. Veja mais na documentação da [Retrofit](https://square.github.io/retrofit/)

2. **GSon** - Biblioteca utilizada para serializar e desserializar objetos Kotlin/Java para JSON e vice versa. Consulte a documentação da [Gson](https://github.com/google/gson) para maiores detalhes.

3.**Lifecycle** - Manipula dados e ciclo de vida de aplicações Android. Para maiores detalhes veja a documentação da  [Jetpack](https://developer.android.com/jetpack/androidx/releases/lifecycle?hl=pt-br)

### Gradle

```
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'
``` 


## Estrutura do Projeto

O projeto está seguindo a estrutura de pacotes abaixo abaixo: 

- **Adapters** - Neste pacote estão dispostos os *adapters* de RecyclerView.
- **Model** - Neste pacote encontram-se as classes de modelo.
- **View** - Neste pacote encontram-se as classes de *Activity* utilizadas no projeto.
- **ViewModel** - Neste pacote encontra-se a classe de *View Model*.
- **WebService** - Neste pacote encontram-se as classes responsáveis por trabalhar com as requisições à API.
- **SupportFiles** - Arquivos de anotações, sem impacto direto ao projeto.


## Testes 

Foi criado um teste (Instrumented Test) para validar as requisições feitas à API. Para maiores detalhes veja a classe *RequestSimpleTest*. Para maiores detalhes a respeito de testes, acesse o link da [documentação de testes](https://developer.android.com/training/testing/unit-testing/instrumented-unit-tests) e também [este aqui](https://www.vogella.com/tutorials/AndroidTesting/article.html). 



## Versão de IDE

Este projeto foi desenvolvido usando a versão 4.1.2 do Android Studio. Recomenda-se utilizar a mesma versão ou superior.  Caso ocorra o erro *What went wrong:This version of the Android Support plugin for IntelliJ IDEA (or Android Studio) cannot open this project, please retrywith version 4.1 or newer.*, pode-se tentar alterar a versão do *com.android.tools.build:gradle* para alguma outra inferior. [Veja este video para maiores detalhes](https://www.youtube.com/watch?v=Mq2aafTgTO8).

### SDK Mínimo: 22 (Android 5.1)
### SDK Target: 30 (Android 11)