package com.edsonjr.chucknorrisfunfact.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.chucknorrisfunfact.Model.JokeModel
import com.edsonjr.chucknorrisfunfact.R

class RecyclerViewSearchJokeByTextAdapter(private val jokes: List<JokeModel>):
    RecyclerView.Adapter<RecyclerViewSearchJokeByTextAdapter.ViewHolder>(){

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val jokeText = view.findViewById(R.id.TXT_jokeQuoteCardView) as TextView

        fun bind(data: String){
            jokeText.text = data

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.joke_quote_list_cell,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return jokes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val jokeTxt = jokes.get(position).value
        holder.bind(jokeTxt)
    }
}