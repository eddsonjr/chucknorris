package com.edsonjr.chucknorrisfunfact.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.chucknorrisfunfact.R
import com.edsonjr.chucknorrisfunfact.View.OnlyOneJokeActivty
import kotlin.coroutines.coroutineContext

class RecyclerViewCategoriesAdapter(private val context: Context,private val categories: List<String>):
    RecyclerView.Adapter<RecyclerViewCategoriesAdapter.ViewHolder>(),RecyclerViewClickListener
{
    private val TAG = "[CategoryRVAdapter]:" //para fins de debug

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val txtCategory = view.findViewById(R.id.TXT_category) as TextView

        fun bind(data: String){
            txtCategory.text = data

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.category_list_cell,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return categories.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories.get(position))
        holder.itemView.setOnClickListener {
            onItemClickListener(categories.get(position))
        }
    }

    override fun <obj> onItemClickListener(data: obj) {
        val categoryStr = data as String
        Log.d(TAG,"Starting OnlyOneJoke with data: $categoryStr")
        val intent = Intent(context,OnlyOneJokeActivty::class.java)
        intent.putExtra("categorySearch",categoryStr)
        context.startActivity(intent)
    }


}