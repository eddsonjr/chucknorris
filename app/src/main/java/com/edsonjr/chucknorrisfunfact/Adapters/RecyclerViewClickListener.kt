package com.edsonjr.chucknorrisfunfact.Adapters

interface RecyclerViewClickListener {

    fun <obj> onItemClickListener(data: obj)
}