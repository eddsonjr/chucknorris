package com.edsonjr.chucknorrisfunfact.WebService

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitSingleton {

    fun initRetrofit(path: String): Retrofit{
        return Retrofit.Builder()
            .baseUrl(path)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}