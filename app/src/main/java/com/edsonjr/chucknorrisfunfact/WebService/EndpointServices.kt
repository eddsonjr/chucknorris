package com.edsonjr.chucknorrisfunfact.WebService

import com.edsonjr.chucknorrisfunfact.Model.JokeModel
import com.edsonjr.chucknorrisfunfact.Model.ListOfJokesModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


//Trabalha com as requisicoes
interface EndpointServices {

    @GET("random")
    fun getRandonFunFact(): Call<JokeModel> //pega somente um fato randomico da api

    @GET("categories")
    fun getListOfCategories(): Call<List<String>>
    //pega a lista de categorias [retorna da api uma lista de strings simples]


    @GET("random?")
    fun queryFunFactByCategorie(
        @Query("category") category: String
    ): Call<JokeModel>
    //retorna um joke com base numa pesquisa feita por categorias


    @GET("search?")
    fun queryFunFactByText(
        @Query("query") text: String
    ): Call<ListOfJokesModel>

}