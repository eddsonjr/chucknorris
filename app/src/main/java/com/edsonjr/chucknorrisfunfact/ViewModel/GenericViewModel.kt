package com.edsonjr.chucknorrisfunfact.ViewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.edsonjr.chucknorrisfunfact.WebService.EndpointServices
import com.edsonjr.chucknorrisfunfact.WebService.RetrofitSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenericViewModel<T> : ViewModel() {

    var liveData:  MutableLiveData<T>? = MutableLiveData()

    private val TAG = "[GenericViewModel]"
    private val baseUrl = "https://api.chucknorris.io/jokes/" //url base
    private val retrofitClient = RetrofitSingleton.initRetrofit(baseUrl)
    private val endpoint = retrofitClient.create(EndpointServices::class.java)



    fun  retrieveDataFromWeb(query: String?, endpointType: String?){

        when(endpointType){
           "jokesByCategory" -> { // usuario esta procurando por uma categoria especifica - Mostrar jokes daquela categoria
               Log.d(TAG,"Usuario procurando por jokes randomicos da categoria $query ")
               val callback = endpoint.queryFunFactByCategorie(query!!)
               setupCallback(callback as Call<T>)
           }

            "random" ->  { //busca randomicamente por jokes
                Log.d(TAG,"Usuario procurando por jokes randomicos ")
                val callback = endpoint.getRandonFunFact()
                setupCallback(callback as Call<T>)
            }

            "onlyListOfCategories" -> { //usuario deseja retornar somente a lista de categorias, sem jokes
                Log.d(TAG,"Usuario adquirindo a lista de categorias...")
                val callback = endpoint.getListOfCategories()
                setupCallback(callback as Call<T>)
            }

            "searchByText" -> { //usuario deseja procurar jokes baseado em texto
                Log.d(TAG,"Usuario procurando por jokes baseados no texto $query ")
                val callback = endpoint.queryFunFactByText(query!!)
                setupCallback(callback as Call<T>)
            }
        }
    }


    private fun  setupCallback(callback: Call<T>) {
        callback.enqueue(object : Callback<T>{
            override fun onFailure(call: Call<T>, t: Throwable) {
                t.printStackTrace()
                Log.d(TAG,"Falha de requisicao - onFailure")
                liveData?.value = null

            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful){
                    Log.d(TAG,"Sucesso na requisicao")
                    liveData?.value =  response.body()!!
                    Log.d(TAG,"Data: ${liveData?.value}")
                }else{
                    Log.d(TAG,"Falha de requisicao")
                    liveData?.value = null
                }
            }
        })
    }



}