package com.edsonjr.chucknorrisfunfact.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.chucknorrisfunfact.Adapters.RecyclerViewCategoriesAdapter
import com.edsonjr.chucknorrisfunfact.R
import com.edsonjr.chucknorrisfunfact.ViewModel.GenericViewModel
import com.edsonjr.chucknorrisfunfact.databinding.ActivityListCategoryBinding

class ListCategoryActivity : AppCompatActivity(),UIUpdaterByObserver {

    private lateinit var binding: ActivityListCategoryBinding
    private val TAG = "[ListCategoryActivity]" //para fins de debug
    private var  genericViewModel: GenericViewModel<List<String>> = GenericViewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //configurando o viewModel e o provider para ele
        genericViewModel = ViewModelProvider(this).get(GenericViewModel::class.java) as GenericViewModel<List<String>>
        genericViewModel.retrieveDataFromWeb(null,"onlyListOfCategories")
        setupViewModelObserver(genericViewModel)



        //configurando a recyclerview
        val layoutManager = LinearLayoutManager(applicationContext)
        binding.cateogiesRecyclerView.layoutManager = layoutManager

    }



    override fun <obj> updateUI(data: obj) {
        val categories = data as List<String>
        Log.d(TAG,"Valor da lista de categorias: $categories")
        binding.cateogiesRecyclerView.adapter = RecyclerViewCategoriesAdapter(this,categories)
        clearUI()


    }


    override fun <obj> setupViewModelObserver(data: obj) {
        val viewModel = data as GenericViewModel<List<String>>
        viewModel.liveData?.observe(this, Observer {
            val data = it
            Log.d(TAG,"Livedata de GenericViewModel: $it")
            if(!it.isNullOrEmpty())
                updateUI(data)
            else {
                Toast.makeText(this, getText(R.string.requestFailMSG), Toast.LENGTH_LONG).show()
                clearUI()
            }

        })
    }

    override fun clearUI() {
        binding.progressBar.visibility = View.GONE
    }


}