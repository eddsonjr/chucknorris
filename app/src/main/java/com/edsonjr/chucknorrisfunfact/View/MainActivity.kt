package com.edsonjr.chucknorrisfunfact.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edsonjr.chucknorrisfunfact.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val TAG = "[MainActivity]" //para fins de debug

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Configurando os eventos de click para os botoes
        binding.BTNSomenteUmJoke.setOnClickListener {
            val intent = Intent(this,OnlyOneJokeActivty::class.java)
            startActivity(intent)
        }

        binding.BTNListaCategorias.setOnClickListener {
            val intent = Intent(this,ListCategoryActivity::class.java)
            startActivity(intent)

        }

        binding.BTNProcurarJokePorTexto.setOnClickListener {
            val intent = Intent(this,SearchJokeByTextActivity::class.java)
            startActivity(intent)
        }

    }

}