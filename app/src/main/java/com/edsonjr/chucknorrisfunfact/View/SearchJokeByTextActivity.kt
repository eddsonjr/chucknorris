package com.edsonjr.chucknorrisfunfact.View


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.chucknorrisfunfact.Adapters.RecyclerViewSearchJokeByTextAdapter
import com.edsonjr.chucknorrisfunfact.Model.JokeModel
import com.edsonjr.chucknorrisfunfact.Model.ListOfJokesModel
import com.edsonjr.chucknorrisfunfact.R
import com.edsonjr.chucknorrisfunfact.ViewModel.GenericViewModel
import com.edsonjr.chucknorrisfunfact.databinding.ActivitySearchJokeByTextBinding

class SearchJokeByTextActivity : AppCompatActivity(),UIUpdaterByObserver {


    private lateinit var binding: ActivitySearchJokeByTextBinding
    private var  genericViewModel: GenericViewModel<ListOfJokesModel> = GenericViewModel()
    private val TAG = "[SearchJokesByTXTACT]"
    private val LIMIT = 50

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchJokeByTextBinding.inflate(layoutInflater)
        setContentView(binding.root)


        clearUI()

        //configurando o viewModel e o provider para ele
        genericViewModel = ViewModelProvider(this).get(GenericViewModel::class.java) as GenericViewModel<ListOfJokesModel>


        //configurando a recyclerView
        val layoutManager = LinearLayoutManager(applicationContext)
        binding.jokesByTextRecyclerView.layoutManager = layoutManager

        setupViewModelObserver(genericViewModel)

        //configurando o click do botao
        binding.BTNSearchJokeByText.setOnClickListener {
            updateUISearching()
            checkUserInputData()
        }

    }


    override fun <obj> updateUI(data: obj) {
        val jokeList = data as ListOfJokesModel

        /*Estipulando uma regra para somente popular a recyclerview com 25 jokes, pois
        * o resultado da query feita na web pode retornar uma quantidade muito grande
        * de jokes*/

        if(jokeList.total >= LIMIT){
            val only25Firsts = jokeList.result.take(LIMIT)
            updateUIDataFound(only25Firsts)
            Toast.makeText(this,"Limitado a $LIMIT itens",Toast.LENGTH_SHORT).show()
        }else if (jokeList.total == 0){
            updateUINotFound()
        }else{
            updateUIDataFound(jokeList.result)
        }


    }

    override fun <obj> setupViewModelObserver(data: obj) {
        val viewModel = data as GenericViewModel<ListOfJokesModel>
        viewModel.liveData?.observe(this, Observer {
            val data = it
            Log.d(TAG,"Livedata de GenericViewModel: $it")
            if(data != null)
                updateUI(data)
            else {
                Toast.makeText(this, getText(R.string.requestFailMSG), Toast.LENGTH_LONG).show()
                clearUI()
            }

        })
    }


    //remove os componentes da tela responsaveis por indicar loading
    override fun clearUI() {
        binding.progressBar.visibility = View.GONE
        binding.TXTSeachJokeInfo.visibility = View.GONE
        binding.jokesByTextRecyclerView.visibility = View.GONE
    }




    //atualiza os componentes da tela para loading
    private fun updateUISearching(){
        binding.progressBar.visibility = View.VISIBLE
        binding.jokesByTextRecyclerView.visibility = View.GONE
        binding.TXTSeachJokeInfo.visibility = View.VISIBLE
        binding.TXTSeachJokeInfo.setText(getText(R.string.onlyOneJokeLoading))

    }




    //atualiza os componentes da tela para caso nao encontrar jokes na pesquisa
    private fun updateUINotFound(){
        binding.progressBar.visibility = View.GONE
        binding.jokesByTextRecyclerView.visibility = View.GONE
        binding.TXTSeachJokeInfo.setText(getText(R.string.noJokesFoundTXT))
    }




    //atualiza os componentes de UI quando os jokes sao encontrados
    private fun updateUIDataFound(listOfJokesModel: List<JokeModel>){
        binding.progressBar.visibility = View.GONE
        binding.TXTSeachJokeInfo.visibility = View.GONE
        binding.jokesByTextRecyclerView.visibility = View.VISIBLE
        binding.jokesByTextRecyclerView.adapter = RecyclerViewSearchJokeByTextAdapter(listOfJokesModel)

    }


    //verifica os dados inseridos pelo usuario, tratando para que ele nao tente fazer pesquisa por strings vazias
    // e tambem tratando possiveis espacos
    private fun checkUserInputData(){
        val text = binding.TXTEditSerchJokeByText.text.toString().trim()

        //verificando se o usuario inseriu algum texto ou nao no campo de texto
        if(text.isNotEmpty()){
            Log.d(TAG,"Usuario procurando jokes por texto: $text")
            genericViewModel.retrieveDataFromWeb(text,"searchByText")
        }else{
            Toast.makeText(this,getText(R.string.insertQuery),Toast.LENGTH_SHORT).show()
            clearUI()
        }
    }




}