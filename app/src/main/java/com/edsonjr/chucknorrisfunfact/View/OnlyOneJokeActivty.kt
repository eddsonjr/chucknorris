package com.edsonjr.chucknorrisfunfact.View

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.edsonjr.chucknorrisfunfact.Model.JokeModel
import com.edsonjr.chucknorrisfunfact.R
import com.edsonjr.chucknorrisfunfact.ViewModel.GenericViewModel
import com.edsonjr.chucknorrisfunfact.databinding.ActivityOnlyOneJokeActivtyBinding


class OnlyOneJokeActivty : AppCompatActivity(),UIUpdaterByObserver {

    private lateinit var binding: ActivityOnlyOneJokeActivtyBinding
    private val TAG = "[OnlyOneJokeACT]:"  //Para fins de debug
    private var  genericViewModel: GenericViewModel<JokeModel> = GenericViewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnlyOneJokeActivtyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        clearUI()

        //configurando o viewModel e o provider para ele
        genericViewModel = ViewModelProvider(this).get(GenericViewModel::class.java) as GenericViewModel<JokeModel>


        //verificando se a chamada desta activity esta ou nao recebendo dados de outra activity
        setupViewModelObserver(genericViewModel)

        //configurando o botao de load joke
        binding.BTNLoadOneJoke.setOnClickListener {
            checkBundle()
        }

    }


    override fun <obj> updateUI(data: obj) {
        val joke = data as JokeModel
        Log.d(TAG,"Atualizando UI com dados que vieram da web...")
        Log.d(TAG,"Joke: ${joke.value}")
        showDataOnUI(joke.value)

    }


    override fun <obj> setupViewModelObserver(data: obj) {
        val viewModel = data as GenericViewModel<JokeModel>
        viewModel.liveData?.observe(this, Observer {
            Log.d(TAG,"Livedata de GenericViewModel: $it")
            if(it != null) {
                val data = it
                updateUI(data)
            }else {
                clearUI()
                Toast.makeText(this, getText(R.string.requestFailMSG), Toast.LENGTH_LONG).show()
                clearUI()
            }

        })
    }



    override fun clearUI() {
        binding.progressBar.visibility = View.GONE
        binding.TXTJoke.visibility = View.GONE
    }



    private fun checkBundle(){
        updateUISearching()
        val categoryNameQuery = intent.getStringExtra("categorySearch")
       if(intent.hasExtra("categorySearch")){
           Log.d(TAG,"Usuario procurando jokes por categoria: $categoryNameQuery")
           Toast.makeText(this,"Procurando por categoria ${categoryNameQuery}",Toast.LENGTH_SHORT).show()
           genericViewModel.retrieveDataFromWeb(categoryNameQuery,"jokesByCategory")
       }else{
           Log.d(TAG,"Jokes randomicos")
           genericViewModel.retrieveDataFromWeb(null,"random")

       }
    }


    private fun updateUISearching(){
        binding.progressBar.visibility = View.VISIBLE
        binding.TXTJoke.visibility = View.VISIBLE
        binding.TXTJoke.setText(getText(R.string.onlyOneJokeLoading))
    }




    private fun showDataOnUI(str: String){
        //Configurando a UI com os valores vindos da web
        binding.progressBar.visibility = View.GONE
        binding.TXTJoke.text = str
    }

}