package com.edsonjr.chucknorrisfunfact.View

import java.util.*

interface UIUpdaterByObserver {

    fun <obj> updateUI(data: obj)
    fun <obj> setupViewModelObserver(data: obj)
    fun clearUI()
}