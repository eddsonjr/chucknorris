package com.edsonjr.chucknorrisfunfact.Model

data class ListOfJokesModel(
        var total: Int,
        var result: MutableList<JokeModel>
)
