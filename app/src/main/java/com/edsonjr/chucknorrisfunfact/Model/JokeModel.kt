package com.edsonjr.chucknorrisfunfact.Model

data class JokeModel(
    var categories: MutableList<String>?,
    var icon_url: String,
    var id: String,
    var url: String,
    var value: String


)