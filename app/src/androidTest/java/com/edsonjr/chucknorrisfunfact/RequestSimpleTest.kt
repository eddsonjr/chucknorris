package com.edsonjr.chucknorrisfunfact

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.edsonjr.chucknorrisfunfact.Model.JokeModel
import com.edsonjr.chucknorrisfunfact.Model.ListOfJokesModel
import com.edsonjr.chucknorrisfunfact.WebService.EndpointServices
import com.edsonjr.chucknorrisfunfact.WebService.RetrofitSingleton
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


@RunWith(AndroidJUnit4::class)
class RequestSimpleTest {

    private val RESPONSE_CODE_OK = 200
    private val CATEGORY_FILTER_NAME  = "animal"
    private val SEARCH_TXT = "sara"
    private val TAG = "[TEST]"


    //Os testes abaixo estao utilizando de estruturas criadas no app base. Para maiores detalhes, verificar as classes e interfaces desenvolvidas
    private val baseUrl = "https://api.chucknorris.io/jokes/" //url base
    private val retrofitClient = RetrofitSingleton.initRetrofit(baseUrl)
    private val response = retrofitClient.create(EndpointServices::class.java)


    //Testando os endpoints

    @Test
    fun testRequestRandon(){
       val responseCode =  response.getRandonFunFact().execute().code()
        Log.d(TAG,responseCode.toString())
        assert(responseCode == RESPONSE_CODE_OK)
    }

    @Test
    fun testRequestListCategories(){
        val responseCode =  response.getListOfCategories().execute().code()
        Log.d(TAG,responseCode.toString())
        assert(responseCode == RESPONSE_CODE_OK)
    }

    @Test
    fun testRequestByCategory(){
        val responseCode =  response.queryFunFactByCategorie(CATEGORY_FILTER_NAME).execute().code()
        Log.d(TAG,responseCode.toString())
        assert(responseCode == RESPONSE_CODE_OK)

    }

    @Test
    fun testRequestSearchText() {
        val responseCode =  response.queryFunFactByText(SEARCH_TXT).execute().code()
        Log.d(TAG,responseCode.toString())
        assert(responseCode == RESPONSE_CODE_OK)
    }


}